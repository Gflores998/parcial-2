/// 
/// Hecho: Gerardo Alexis Flores Mejia
/// Carnet: 25-0036-2017
/// Carrera: Ingeniería en Sistemas
/// 
import 'package:flutter/material.dart';
import 'package:parcial_2/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lista de estudiantes',
      home: HomePage(),
      theme: ThemeData(
        scaffoldBackgroundColor: Color.fromRGBO(238, 238, 243, 1)
      ),
    );
  }

}
