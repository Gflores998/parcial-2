/// 
/// Hecho: Gerardo Alexis Flores Mejia
/// Carnet: 25-0036-2017
/// Carrera: Ingeniería en Sistemas
/// 

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:parcial_2/models/alumnos.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {

  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  late Future<List<ApiAlumnos>> _allStudents;

  Future<List<ApiAlumnos>> _getData() async {
    final response = await http.get(
      Uri.parse('https://utecclass.000webhostapp.com/post.php')
    );

    List<ApiAlumnos> students = [];

    String cuerpo;
    if (response.statusCode == 200) {
        cuerpo = utf8.decode(response.bodyBytes);
        final data = jsonDecode(cuerpo);

        for (var alumno in data) {
          students.add(
            ApiAlumnos(alumno['title'], alumno['content'])
          );
        }
    } else {
      throw Exception('No se obtuvo información');
    }
    return students;
  }

  @override
  void initState() {
    _allStudents =  _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de estudiantes', 
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        backgroundColor: Color.fromRGBO(33, 98, 238, 1),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () {}, 
            icon: Icon(
              Icons.home,
              color: Colors.white,
            )
          )
        ],
      ),
      body: FutureBuilder(
        future: _allStudents,
        builder: (_, snapshot) {
          if (snapshot.hasData) {
            return ListView(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0 ),
              children: listaCompleta(snapshot.data),
            );
          } else if(snapshot.hasError) {
            return Center(
              child: Text('Error al mostrar la información'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      )
    );
  }

  List<Widget> listaCompleta(data) {
    List<Widget> cards = [];

    for(var item in data) {
      cards.add(
        Card(
          elevation: 0,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                leading: CircleAvatar(
                  child: Text(
                    item.title[0],
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                    ),
                  ),
                  backgroundColor: Color.fromRGBO(33, 98, 238, 1),
                ),
                title: Text(item.content, 
                  style: TextStyle(
                    color: Color.fromRGBO(46, 61, 94, 1),
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0
                  ),
                ),
                subtitle: Text(item.title,
                  style: TextStyle(
                    color: Color.fromRGBO(186, 195, 215, 1),
                    fontSize: 16.0
                  ),
                ),
              )
            ],
          ),
        )
      );
      cards.add(
        SizedBox(
          height: 10.0,
        )
      );
    }

    return cards;

  }

}