# Parcial 2 - Flutter

La pantalla contiene lo siguiente:
 - Slide datos
 - Slide Apellidos
 - Consumo de API Rest

Desarrollar la pantalla anterior tomando como ejemplo:

### Puntos completados
- [x] Ajustar la plantilla según las medidas
``` terminal
La pantalla se adapta completamente ademas utiliza scroll para navegar a los resultados más bajos
```

- [x] La cabecera debera contener un icono
``` terminal
Se agregó el icono de "Home" en la parte superior izquierda
```

- [x] El fondo a colocar es de su preferencia
``` terminal
Para este caso del parcial, se optó por no utilizar una imagen de fondo sino que hacer uso de un color gris 
claro para darle un resalte a las card con la información
```

- [x] La pantalla tiene que ser responsiva

``` terminal
La app muestra la información apropiadamente tanto en modo landscape como portrait (Ver imagen 2)
```

- [x] Se evaluará creatividad
``` terminal
Toque personal: Se agrega un avatar dinamico con la inicial de cada nombre de la lista
```

- [x] Se tomará en cuenta como evaluación los fondos, colores respectivos para la pantalla
``` terminal
Se hizó uso de tres colores para la app, el color primario azul claro (appBar y avatar), 
azul oscuro (Texto del titulo) y gris claro (Texto para el nombre) y como 
background se opto por NO utilizar imagen sino un color que combine y resalte lo blanco de las cards!
```


### Pantalla de la aplicación

<img src="pantalla.png" alt="drawing" width="200"/>

Vista Responsiva

![Pantalla](./pantalla2.png)